#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <cctype>
//#include <ctime>
// using name space
using namespace std;

struct spriteSizeNode{
	//int mNumber;
	static int size;
	int width;
	int height;
	int pos_x;
	int pos_y;
	int sheet_num;
	struct spriteSizeNode *next;
	spriteSizeNode():next(NULL),width(0),height(0),pos_x(0),pos_y(0),sheet_num(0){}
};
int spriteSizeNode::size = 0;
typedef spriteSizeNode* SizeNode;
enum READ_STATE{
	WIDTH,
	X,
	HEIGHT,
	IDLE
};
// create sprite size imformation link list from a text file
void getSpriteListFromFile(SizeNode &head_node, string file_name ){
	if( head_node != NULL ){
		cerr<<"error : the head node is not empty.";
		return;
	}else{
		// node ptr
		spriteSizeNode *node_ptr = new spriteSizeNode;
		head_node = node_ptr;
		

		// load from file
		cout<<"\nLoad....";
		ifstream mLoadSizeInfor;
		mLoadSizeInfor.close();
		mLoadSizeInfor.clear();
		mLoadSizeInfor.open( file_name.c_str() );
		
		// process
		if( mLoadSizeInfor )
		{
			READ_STATE mRead = READ_STATE::IDLE;
			// read conditions
			char content;
			
			//int precise = 0;
			int number = 0;
			// reading
			while( !mLoadSizeInfor.eof() )
			{
				// keep read from file
				content = mLoadSizeInfor.get();    
            
				if( content != ' ' && content != '\t' && content !='\n' && content != -1 ){
					if( isdigit(content) ){
						if( mRead == READ_STATE::IDLE ){
							// it is a new size information
							// create a new node
							node_ptr->next = new spriteSizeNode;
							spriteSizeNode::size++;
							node_ptr = node_ptr->next;
							// store the number
							number = content - '0';
							// change the state
							mRead = READ_STATE::WIDTH;
						}else if( mRead == READ_STATE::WIDTH ){
							//precise ++;
							int temp_number = content - '0';
							number = number*10 + temp_number;
						}else if( mRead == READ_STATE::HEIGHT ){
							//precise ++;
							int temp_number = content - '0';
							number = number*10 + temp_number;
						}else if( mRead == READ_STATE::X ){
							//precise = 0;
							mRead = READ_STATE::HEIGHT;
							number = content - '0';
						}
					}else if( content == 'x'){
						mRead = READ_STATE::X;
						//save the width
						node_ptr->width = number;
						number = 0;
					}
				}else{
					//precise = 0;
					if( mRead == READ_STATE::HEIGHT ){
						mRead = READ_STATE::IDLE;
						node_ptr->height = number;
						number = 0;
					}else if( mRead == READ_STATE::IDLE ){
					}else{
						cerr<<"error : the text format is not correct.";
						return;
					}
				}
			}
			if( mRead == READ_STATE::HEIGHT ){
				mRead = READ_STATE::IDLE;
				node_ptr->height = number;
				number = 0;
			}

			mLoadSizeInfor.close();
			cout<<"\nLoading Complete.";
			head_node = head_node->next;
			return;
		}
		else
		{
			std::cerr<<"error : can not load.";
			return;
		}
	}
}
void calculate(spriteSizeNode *node_ptr){
	spriteSizeNode *node = node_ptr;
	for(int i = 0; i <= spriteSizeNode::size-1; i ++){
		spriteSizeNode *node = node_ptr;
		while(node->next){
			if(node->width>node->next->width){
				int width = node->width;
				int height = node->height;

				node->width = node->next->width;
				node->height = node->next->height;
				node->next->width = width;
				node->next->height = height;
			}else if(node->width == node->next->width){
				if(node->height>node->next->height){
					int width = node->width;
					int height = node->height;

					node->width = node->next->width;
					node->height = node->next->height;
					node->next->width = width;
					node->next->height = height;
				}
			}
			node = node->next;
		}
	}
	
}

const int SHEET_WIDTH = 1024;
const int SHEET_HEIGHT = 1024;
void store(spriteSizeNode *node_ptr){
	spriteSizeNode *node = node_ptr;
	int sheetNum = 1;
	// save the replay
    //int used_width = 0;
	int used_height = 0;
	int pre_width = 0;
	int pre_height = 0;
	
	while(node){
		while(node && used_height+node->height <= SHEET_HEIGHT){
			pre_width = 0;
			pre_height = used_height;
			while(node && pre_width + node->width <= SHEET_WIDTH){
				node->pos_x = pre_width;
				node->pos_y = pre_height;
				node->sheet_num = sheetNum;
				if(used_height < (node->height+pre_height)){
					used_height = (node->height+pre_height);
				}
				pre_width = node->pos_x+node->width;
				pre_height =node->pos_y;
				node = node->next;
			}	
		}
		used_height = 0;
		pre_width = 0;
		pre_height = 0;
		sheetNum++;
	}
	
	node = node_ptr;
	for(int i = 1; i < sheetNum; i ++){
		std::ofstream mSaveSheet;
		mSaveSheet.close();
		mSaveSheet.clear();
		string name = string("sheet" + to_string(i));
		mSaveSheet.open( name.c_str() );
		if( mSaveSheet )
		{
				mSaveSheet<<"sheet"<<i<<"\n";

				while(node&&node->sheet_num == i){
					mSaveSheet<<node->width<<"x"<<node->height<<" "<<node->pos_x<<" "<<node->pos_y<<"\n";
					node = node->next;
				}

				mSaveSheet.close();
		}
		else
		{
				std::cerr<<"error : can not save.";
				return;
		}
	}
	
}
int main()
{
        // print the title
        cout<<"Xinlei Cao"<<endl;
        cout<<"============================================="<<endl;
        cout<<"====        SPRITE SHEET PACKER          ===="<<endl;
        cout<<"============================================="<<endl;
        


		spriteSizeNode *head_node = NULL;//nullptr

		getSpriteListFromFile(head_node, "input.txt" );
		calculate(head_node);
		store(head_node);


        // pause when debugging
        //std::system( "pause" );
        std::string message;
        cout<<"\n\n\nleave a message and exit............"<<endl;
        cin>>message;
        // end - of - main function
        return 0;
}